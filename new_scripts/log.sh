#!/bin/bash
#
#
LOG_FILE="skript.log"

log_message() {
    local MESSAGE="$1"
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $MESSAGE" | tee - a "$LOG_FILE"

} 

log_message "script is starting" 


if test -f "/etc/passwd" 

then 
	log_message "file is ok" 

else 
	log_message "file is not ok" 

fi 

log_message "end of script" 
