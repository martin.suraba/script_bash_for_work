#!/bin/bash

echo "install icinga2"

yum update -y && yum upgrade -y

yum install -y https://packages.icinga.com/epel/icinga-rpm-release-7-latest.noarch.rpm

yum install -y epel-release

yum update -y

sleep 20

yum install -y icinga2
systemctl enable icinga2
systemctl start icinga2
systemctl status icinga2


yum install -y nagios-plugins-all

icinga2 daemon -C


firewall-cmd --add-service=http && firewall-cmd --permanent --add-service=http


yum install -y vim-icinga2


yum install -y mariadb-server mariadb

systemctl enable mariadb
systemctl start mariadb

yum install icinga2-ido-mysql


icinga2 feature enable ido-mysql


systemctl restart icinga2


yum install -y httpd
systemctl enable httpd
systemctl start httpd

icinga2 api setup


systemctl restart icinga2


yum install -y centos-release-scl


yum install -y icingaweb2 icingacli



yum install -y icingaweb2-selinux


yum install -y rh-php73-php-mysqlnd rh-php73-php-fpm sclo-php73-php-pecl-imagick rh-php73-php-ldap rh-php73-php-pgsql rh-php73-php-xmlrpc rh-php73-php-intl rh-php73-php-gd rh-php73-php-pdo rh-php73-php-soap rh-php73-php-posix rh-php73-php-cli


systemctl start rh-php73-php-fpm.service
systemctl enable rh-php73-php-fpm.service
systemctl restart httpd
systemctl restart rh-php73-php-fpm.service
