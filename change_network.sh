
#!/bin/bash 

ip link set ens224 down

ip link set ens224 name eth1 

ip link eth1 up 

ip link set ens192 down

ip link set ens192 name eth0 

ip link eth0 up 

systemctl restart network

systemctl status network 
